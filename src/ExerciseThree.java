public class ExerciseThree {

    public void start() {

       /* System.out.println(foo(4));*/
//        System.out.println(bar(2, 3));
//        System.out.println(bar(3, -2));
//        System.out.println(bad2(10));
        System.out.println(bad3(2));
    }

    private int foo(int x) {
        if (x <= 1) {
            return 1;
        }
        return x * foo(x - 1);
    }

    private double bar(double x, int n) {
        if (n > 1)
            return x * bar(x, n - 1);
        else if (n < 0)
            return 1.0 / bar(x, -n);
        else
            return x;
    }

    private void bad1() {
        System.out.println("This is very good code.");
        bad1();
    }

    private int bad2(int n) {
        if (n == 0) {
            return 0;
        }
        return n + bad2(n - 2);
    }

    private int bad3(int n) {
        if (n == 0) {
            return 0;
        }
        return n + bad3(n + 1);
    }


    public static void main(String[] args) {
        ExerciseThree p = new ExerciseThree();
        p.start();
    }
}
